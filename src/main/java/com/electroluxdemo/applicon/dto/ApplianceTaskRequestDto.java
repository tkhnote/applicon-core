package com.electroluxdemo.applicon.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class ApplianceTaskRequestDto {
	@NotNull
	private String taskName;
}
