create schema applicon;

create table applicon.appliance_list (
  id           uuid         not null default random_uuid() primary key,
  name         varchar(255) not null,
  type         varchar(255) not null,
  status       varchar(255) not null,
  current_task varchar(255) not null
);

create table applicon.appliance_task_history (
  id             uuid         not null default random_uuid() primary key,
  appliance_name varchar(255) not null,
  appliance_type varchar(255) not null,
  appliance_task varchar(255) not null,
  date           varchar(255) not null
);

