# Applicon 

## Overview

Example of service that simulates the appliances control.

## Getting started

```
1. clone the repository: git clone https://tkhnote@bitbucket.org/tkhnote/applicon-core.git
2. navigate to 'applicon-core' directory via terminal
3. mvn clean package
4. cd target
5. java -jar applicon-core-1.0.0-SNAPSHOT.jar
```

## API requests

### 1. POST /appliance/add 

Add new appliance. By default there are none.

Available types: WASH_MACHINE, OVEN, FRIDGE, COMMON_APPLIANCE, CUSTOM. 

Request body example:
```
{
	"type":"FRIDGE",
	"name":"F1"
}
```

Response example:
```
{
    "id": "165a91cb-aa8e-46a3-a75c-62fe1c0cd0f0",
    "name": "F1",
    "type": "Fridge",
    "status": "Waiting for task",
    "currentTask": "No task"
}
```

### 2. GET /appliance/appliance_list

Get list of added appliances.

Response example:
```
[
    {
        "id": "d74db170-9d7b-492a-9967-4a6033d48127",
        "name": "F1",
        "type": "Fridge",
        "status": "Waiting for task",
        "currentTask": "No task"
    },
    {
        "id": "91af4e3b-3f8e-4275-9b14-28a80e21930c",
        "name": "F2",
        "type": "Fridge",
        "status": "Waiting for task",
        "currentTask": "No task"
    }
]
```

### 3. GET /appliance/{applianceId}

Get information about specific appliance by it's ID.

Response example:
```
{
    "id": "d74db170-9d7b-492a-9967-4a6033d48127",
    "name": "F1",
    "type": "Fridge",
    "status": "Waiting for task",
    "currentTask": "No task"
}
```

### 4. PUT /appliance/{applianceId}/launch

Launch appliance by ID with some task, status will be changed to "Work in progress".

Available tasks (grouped by appliance type): 
	WASH_MACHINE: "Washing", "Tumble dry"),
	FRIDGE: "Deep frost", "Defrost",
	OVEN: "Grill", "Heat and fan", "Microwave",
	COMMON_APPLIANCE: "No task".

Request body example:
```
{
	"taskName" : "Defrost"
}
```

Response example:
```
{
    "id": "165a91cb-aa8e-46a3-a75c-62fe1c0cd0f0",
    "name": "F1",
    "type": "Fridge",
    "status": "Work in progress",
    "currentTask": "Defrost"
}
```

### 5. PUT /appliance/{applianceId}/stop

Stop appliance by ID, status will be changed to "Waiting for task".

Response example:
```
{
    "id": "d74db170-9d7b-492a-9967-4a6033d48127",
    "name": "F1",
    "type": "Fridge",
    "status": "Waiting for task",
    "currentTask": "No task"
}
```

### 6. GET /appliance/appliance_task_history

Get history of launch/stop requests for current application run.

Response example:
```
[
    {
        "id": "c947ebc7-a00e-46ac-8ba5-9b9ebea19475",
        "applianceName": "F1",
        "applianceType": "Fridge",
        "applianceTask": "No task",
        "date": "March 3, 2020 4:35:20 AM MSK"
    },
    {
        "id": "62d335e1-5677-4e2c-9d29-75068d288af6",
        "applianceName": "F1",
        "applianceType": "Fridge",
        "applianceTask": "Defrost",
        "date": "March 3, 2020 4:38:18 AM MSK"
    },
    {
        "id": "6f33f4f7-3ede-487f-bfb3-a83300b5df3f",
        "applianceName": "F1",
        "applianceType": "Fridge",
        "applianceTask": "Deep frost",
        "date": "March 3, 2020 4:38:30 AM MSK"
    }
]
```

## Usage examples (with curl)

### 0. IMPORTANT! Your ID values will be different from mine as they're generated on the fly, so in http links it should be repalaced by yours.

### 1. Add new appliance (e.g., with type = "FRIDGE" and name = "F2")

```
curl --request POST 'http://localhost:8080/appliance/add' --header 'Content-Type: application/json' --data-raw '{"type":"FRIDGE","name":"F2"}'
```

Response example: 
```
{
    "id": "008f5526-b943-4a16-8877-9b8707b3487c",
    "name": "F2",
    "type": "Fridge",
    "status": "Waiting for task",
    "currentTask": "No task"
}

```

### 2. Get list of added appliances - there should be the one you've added

```
curl 'http://localhost:8080/appliance/appliance_list'
```

Response example:
```
[
    {
        "id": "008f5526-b943-4a16-8877-9b8707b3487c",
		"name": "F2",
		"type": "Fridge",
		"status": "Waiting for task",
		"currentTask": "No task"
    }
]
```

### 3. Get information about added appliance

```
curl 'http://localhost:8080/appliance/008f5526-b943-4a16-8877-9b8707b3487c'
```

Response example:
```
{
    "id": "008f5526-b943-4a16-8877-9b8707b3487c",
    "name": "F2",
    "type": "Fridge",
    "status": "Waiting for task",
    "currentTask": "No task"
}
```


### 4. Launch appliance with some task

```
curl --request PUT 'http://localhost:8080/appliance/008f5526-b943-4a16-8877-9b8707b3487c/launch' --header 'Content-Type: application/json' --data-raw '{"taskName" : "Deep frost"}' 
```

Response example:
```
{
    "id": "008f5526-b943-4a16-8877-9b8707b3487c",
    "name": "F2",
    "type": "Fridge",
    "status": "Work in progress",
    "currentTask": "Deep frost"
}
```

### 5. Stop appliance

```
curl --request PUT 'http://localhost:8080/appliance/008f5526-b943-4a16-8877-9b8707b3487c/stop' 
```

Response example:
```
{
    "id": "008f5526-b943-4a16-8877-9b8707b3487c",
    "name": "F2",
    "type": "Fridge",
    "status": "Waiting for task",
    "currentTask": "No task"
}
```

### 6. Get list of launch/stop requests

```
curl 'http://localhost:8080/appliance/appliance_task_history'
```

Response example: 
```
[
    {
        "id": "008f5526-b943-4a16-8877-9b8707b3487c",
        "applianceName": "F2",
        "applianceType": "Fridge",
        "applianceTask": "Deep frost",
        "date": "March 3, 2020 4:35:20 AM MSK"
    },
    {
        "id": "008f5526-b943-4a16-8877-9b8707b3487c",
        "applianceName": "F2",
        "applianceType": "Fridge",
        "applianceTask": "No task",
        "date": "March 3, 2020 4:35:31 AM MSK"
    }
]
```