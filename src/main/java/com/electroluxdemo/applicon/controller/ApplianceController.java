package com.electroluxdemo.applicon.controller;


import com.electroluxdemo.applicon.dto.ApplianceAddRequestDto;
import com.electroluxdemo.applicon.dto.ApplianceResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskHistoryResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskRequestDto;
import com.electroluxdemo.applicon.service.ApplianceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/appliance")
public class ApplianceController {

	private final ApplianceService applianceService;

	public ApplianceController(ApplianceService applianceService) {
		this.applianceService = applianceService;
	}

	@PostMapping("/add")
	public ApplianceResponseDto addAppliance(@Valid @RequestBody ApplianceAddRequestDto applianceAddRequestDto) {
		return applianceService.addAppliance(applianceAddRequestDto);
	}

	@GetMapping("/appliance_list")
	public List<ApplianceResponseDto> getApplianceList() {
		return applianceService.getApplianceList();
	}

	@GetMapping("/{applianceId}")
	public ApplianceResponseDto getApplianceById(@PathVariable UUID applianceId) {
		return applianceService.getApplianceDtoById(applianceId);
	}

	@PutMapping("/{applianceId}/launch")
	public ApplianceResponseDto launchAppliance(@PathVariable UUID applianceId, @Valid @RequestBody ApplianceTaskRequestDto request) {
		return applianceService.launchApplianceTask(applianceId, request);
	}

	@PutMapping("/{applianceId}/stop")
	public ApplianceResponseDto stopAppliance(@PathVariable UUID applianceId) {
		return applianceService.stopAppliance(applianceId);
	}

	@GetMapping("/appliance_task_history")
	public List<ApplianceTaskHistoryResponseDto> getTaskHistory() {
		return applianceService.getTaskHistory();
	}

}
