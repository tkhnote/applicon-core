package com.electroluxdemo.applicon.enums;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum ApplianceTask {
	WASH(ApplianceType.WASH_MACHINE, "Washing"),
	TUMBLE_DRY(ApplianceType.WASH_MACHINE, "Tumble dry"),
	DEEP_FROST(ApplianceType.FRIDGE, "Deep frost"),
	DEFROST(ApplianceType.FRIDGE, "Defrost"),
	GRILL(ApplianceType.OVEN, "Grill"),
	HEAT_AND_FAN(ApplianceType.OVEN, "Heat and fan"),
	MICROWAVE(ApplianceType.OVEN, "Microwave"),
	NO_TASK(ApplianceType.COMMON_APPLIANCE, "No task");

	private ApplianceType applianceType;
	private String taskName;

	ApplianceTask(ApplianceType applianceType, String taskName) {
		this.applianceType = applianceType;
		this.taskName = taskName;
	}

	public static ApplianceTask getApplianceTaskByNameAndCheckType(String name, ApplianceType type) {
		return Stream.of(ApplianceTask.values())
					 .filter(task -> task.getTaskName().equals(name))
					 .filter(task -> task.getApplianceType().equals(ApplianceType.COMMON_APPLIANCE) ||
									 task.getApplianceType().equals(type))
					 .findAny()
					 .orElseThrow(() -> new IllegalStateException(String.format("Illegal task name (%s) or it doesn't " +
																				"match chosen appliance (%s)",
																				name,
																				type.getTypeName())));
	}
}
