package com.electroluxdemo.applicon.enums;

import lombok.Getter;

@Getter
public enum ApplianceType {
	WASH_MACHINE("Washing machine"),
	OVEN("Oven"),
	FRIDGE("Fridge"),
	COMMON_APPLIANCE("Common appliance"),
	CUSTOM("Custom appliance");

	String typeName;

	ApplianceType(String typeName) {
		this.typeName = typeName;
	}

}
