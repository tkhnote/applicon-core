package com.electroluxdemo.applicon.enums;

import lombok.Getter;

@Getter
public enum ApplianceStatus {
	WORK("Work in progress"),
	WAIT("Waiting for task");

	String statusName;

	ApplianceStatus(String statusName) {
		this.statusName = statusName;
	}
}
