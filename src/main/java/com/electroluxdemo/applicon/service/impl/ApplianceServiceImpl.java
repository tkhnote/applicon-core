package com.electroluxdemo.applicon.service.impl;

import com.electroluxdemo.applicon.dto.ApplianceAddRequestDto;
import com.electroluxdemo.applicon.dto.ApplianceResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskHistoryResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskRequestDto;
import com.electroluxdemo.applicon.entity.Appliance;
import com.electroluxdemo.applicon.entity.ApplianceTaskHistory;
import com.electroluxdemo.applicon.enums.ApplianceStatus;
import com.electroluxdemo.applicon.enums.ApplianceTask;
import com.electroluxdemo.applicon.repository.ApplianceRepo;
import com.electroluxdemo.applicon.repository.ApplianceTaskHistoryRepo;
import com.electroluxdemo.applicon.service.ApplianceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ApplianceServiceImpl implements ApplianceService {
	private final ApplianceRepo applianceRepo;
	private final ApplianceTaskHistoryRepo applianceTaskHistoryRepo;

	@Transactional
	public ApplianceResponseDto addAppliance(ApplianceAddRequestDto applianceAddRequestDto) {
		return applianceRepo.save(Appliance.builder()
										   .name(applianceAddRequestDto.getName())
										   .type(applianceAddRequestDto.getType())
										   .status(ApplianceStatus.WAIT)
										   .currentTask(ApplianceTask.NO_TASK)
										   .build()).toResponseDto();
	}

	public List<ApplianceResponseDto> getApplianceList() {
		return applianceRepo.findAll().stream().map(Appliance::toResponseDto).collect(Collectors.toList());
	}


	public ApplianceResponseDto getApplianceDtoById(UUID applianceUuid) {
		return getApplianceById(applianceUuid).toResponseDto();
	}

	@Transactional
	public ApplianceResponseDto launchApplianceTask(UUID applianceUuid, ApplianceTaskRequestDto requestDto) {
		final Appliance currentAppliance = getApplianceById(applianceUuid);
		return commandAppliance(currentAppliance,
								ApplianceStatus.WORK,
								ApplianceTask.getApplianceTaskByNameAndCheckType(requestDto.getTaskName(),
																				 currentAppliance.getType())).toResponseDto();
	}

	@Transactional
	public ApplianceResponseDto stopAppliance(UUID applianceUuid) {
		final Appliance currentAppliance = getApplianceById(applianceUuid);
		return commandAppliance(currentAppliance, ApplianceStatus.WAIT, ApplianceTask.NO_TASK).toResponseDto();
	}

	public List<ApplianceTaskHistoryResponseDto> getTaskHistory() {
		return applianceTaskHistoryRepo.findAll()
									   .stream()
									   .map(ApplianceTaskHistory::toResponseDto)
									   .sorted(Comparator.comparing(ApplianceTaskHistoryResponseDto::getDate))
									   .collect(Collectors.toList());
	}

	private Appliance getApplianceById(UUID applianceUuid) {
		return applianceRepo.findById(applianceUuid)
							.orElseThrow(() -> new EntityNotFoundException(String.format("Can't find appliance by id %s",
																						 applianceUuid)));
	}


	private Appliance commandAppliance(Appliance appliance, ApplianceStatus status, ApplianceTask task) {
		Appliance result = applianceRepo.save(appliance.withStatus(status).withCurrentTask(task));
		saveTaskHistory(result);
		return result;
	}

	private void saveTaskHistory(Appliance appliance) {
		applianceTaskHistoryRepo.save(new ApplianceTaskHistory().withApplianceName(appliance.getName())
																.withApplianceType(appliance.getType())
																.withApplianceTask(appliance.getCurrentTask()));
	}

}
