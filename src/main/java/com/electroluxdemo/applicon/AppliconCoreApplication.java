package com.electroluxdemo.applicon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppliconCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppliconCoreApplication.class, args);
	}

}
