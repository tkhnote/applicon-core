package com.electroluxdemo.applicon.service;

import com.electroluxdemo.applicon.dto.ApplianceAddRequestDto;
import com.electroluxdemo.applicon.dto.ApplianceResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskHistoryResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskRequestDto;

import java.util.List;
import java.util.UUID;

public interface ApplianceService {

	List<ApplianceResponseDto> getApplianceList();

	ApplianceResponseDto addAppliance(ApplianceAddRequestDto appliance);

	ApplianceResponseDto getApplianceDtoById(UUID applianceUuid);

	ApplianceResponseDto launchApplianceTask(UUID applianceUuid, ApplianceTaskRequestDto requestDto);

	ApplianceResponseDto stopAppliance(UUID applianceUuid);

	List<ApplianceTaskHistoryResponseDto> getTaskHistory();

}
