package com.electroluxdemo.applicon.entity;

import com.electroluxdemo.applicon.dto.ApplianceResponseDto;
import com.electroluxdemo.applicon.enums.ApplianceStatus;
import com.electroluxdemo.applicon.enums.ApplianceTask;
import com.electroluxdemo.applicon.enums.ApplianceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Entity
@Table(name = "appliance_list", schema = "applicon")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@With
public class Appliance {
	@Id
	@GeneratedValue
	private UUID id;

	private String name;

	private ApplianceType type;

	private ApplianceStatus status;

	private ApplianceTask currentTask;

	public ApplianceResponseDto toResponseDto() {
		return ApplianceResponseDto.builder()
								   .id(this.id)
								   .name(this.name)
								   .type(this.type.getTypeName())
								   .status(this.status.getStatusName())
								   .currentTask(this.currentTask.getTaskName())
								   .build();
	}
}
