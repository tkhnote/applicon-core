package com.electroluxdemo.applicon.service;

import com.electroluxdemo.applicon.dto.ApplianceAddRequestDto;
import com.electroluxdemo.applicon.dto.ApplianceResponseDto;
import com.electroluxdemo.applicon.dto.ApplianceTaskRequestDto;
import com.electroluxdemo.applicon.enums.ApplianceTask;
import com.electroluxdemo.applicon.enums.ApplianceType;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
class ApplianceServiceTest {

	private static final UUID UUID_TEMPLATE = UUID.randomUUID();

	@Mock
	private ApplianceAddRequestDto applianceAddRequestDto;

	@Mock
	private ApplianceTaskRequestDto applianceTaskRequestDto;

	@Mock
	private ApplianceService applianceService;

	@Test
	void testAddAppliance() {
		when(applianceService.addAppliance(any())).thenReturn(new ApplianceResponseDto().toBuilder()
																						.id(UUID_TEMPLATE)
																						.build());
		ApplianceResponseDto appliance = applianceService.addAppliance(applianceAddRequestDto);
		Assert.assertEquals(UUID_TEMPLATE, appliance.getId());
	}

	@Test
	void testLaunchApplianceTask() {
		when(applianceService.launchApplianceTask(any(), any()))
				.thenReturn(new ApplianceResponseDto().toBuilder()
													  .id(UUID_TEMPLATE)
													  .currentTask(ApplianceTask.GRILL.getTaskName())
													  .type(ApplianceType.OVEN.getTypeName())
													  .build());
		ApplianceResponseDto appliance = applianceService.launchApplianceTask(UUID_TEMPLATE, applianceTaskRequestDto);
		Assert.assertEquals(UUID_TEMPLATE, appliance.getId());
		Assert.assertEquals(ApplianceTask.GRILL.getTaskName(), appliance.getCurrentTask());
		Assert.assertEquals(ApplianceType.OVEN.getTypeName(), appliance.getType());
	}


}
