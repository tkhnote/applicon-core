package com.electroluxdemo.applicon.repository;

import com.electroluxdemo.applicon.entity.Appliance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ApplianceRepo extends JpaRepository<Appliance, UUID> {
}
