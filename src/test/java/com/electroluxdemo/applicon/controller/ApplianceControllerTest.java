package com.electroluxdemo.applicon.controller;

import com.electroluxdemo.applicon.dto.ApplianceResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ApplianceControllerTest {

	@Autowired
	ObjectMapper obm;

	@Autowired
	private MockMvc mock;

	@Test
	void testAddAppliance() throws Exception {

		mock.perform(post("/appliance/add").content("{\"type\":\"FRIDGE\", \"name\":\"F1\"}")
										   .contentType(MediaType.APPLICATION_JSON_VALUE))
			.andExpect(jsonPath("$.id").isNotEmpty())
			.andExpect(status().isOk());
	}

	@Test
	void testGetApplianceDtoById() throws Exception {

		final String stringResponse =
				mock.perform(post("/appliance/add").content("{\"type\":\"FRIDGE\", \"name\":\"F1\"}")
												   .contentType(MediaType.APPLICATION_JSON_VALUE))
					.andReturn()
					.getResponse()
					.getContentAsString();

		final ApplianceResponseDto applianceResponseDto = obm.readValue(stringResponse, ApplianceResponseDto.class);

		mock.perform(get("/appliance//{applianceId}", applianceResponseDto.getId())).andExpect(status().isOk());
	}
}
