package com.electroluxdemo.applicon.repository;

import com.electroluxdemo.applicon.entity.ApplianceTaskHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ApplianceTaskHistoryRepo extends JpaRepository<ApplianceTaskHistory, UUID> {
}
