package com.electroluxdemo.applicon.dto;

import com.electroluxdemo.applicon.enums.ApplianceType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Builder(toBuilder = true)
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ApplianceAddRequestDto {
	@NotNull
	private ApplianceType type;
	@NotNull
	private String name;
}
