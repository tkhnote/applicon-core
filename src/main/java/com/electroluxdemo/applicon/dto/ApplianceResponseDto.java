package com.electroluxdemo.applicon.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static com.fasterxml.jackson.annotation.JsonInclude.*;

@Builder(toBuilder = true)
@Getter
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ApplianceResponseDto {
	private UUID id;
	private String name;
	private String type;
	private String status;
	private String currentTask;
}
