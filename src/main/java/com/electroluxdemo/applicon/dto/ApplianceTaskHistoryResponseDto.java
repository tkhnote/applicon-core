package com.electroluxdemo.applicon.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class ApplianceTaskHistoryResponseDto {
	UUID id;
	String applianceName;
	String applianceType;
	String applianceTask;
	String date;
}
