package com.electroluxdemo.applicon.entity;

import com.electroluxdemo.applicon.dto.ApplianceTaskHistoryResponseDto;
import com.electroluxdemo.applicon.enums.ApplianceTask;
import com.electroluxdemo.applicon.enums.ApplianceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.UUID;

@Getter
@Entity
@Table(name = "appliance_task_history", schema = "applicon")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@With
public class ApplianceTaskHistory {

	@Id
	@GeneratedValue
	UUID id;

	@Column(name = "appliance_name")
	String applianceName;

	@Column(name = "appliance_type")
	ApplianceType applianceType;

	@Column(name = "appliance_task")
	ApplianceTask applianceTask;

	Instant date = Instant.now();

	public ApplianceTaskHistoryResponseDto toResponseDto() {
		return new ApplianceTaskHistoryResponseDto(this.id,
												   this.applianceName,
												   this.applianceType.getTypeName(),
												   this.applianceTask.getTaskName(),
												   DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
																	.withLocale(Locale.getDefault())
																	.withZone(ZoneId.systemDefault())
																	.format(this.date));
	}

}
